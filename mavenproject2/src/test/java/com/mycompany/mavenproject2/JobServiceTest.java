/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject2;

import java.time.LocalDate;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author PP
 */
public class JobServiceTest {

    public JobServiceTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of checkEnableTime method, of class JobService.
     */
    @Test
    public void testCheckEnableTimeTodayIsBetwwnStartTimeAndEndTime() {
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021, 2, 10);
        LocalDate endTime = LocalDate.of(2021, 2, 14);
        LocalDate today = LocalDate.of(2021, 2, 11);
        boolean expResult = true;
        //Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        //Assert
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckEnableTimeTodayEquleStartTime() {
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021, 2, 10);
        LocalDate endTime = LocalDate.of(2021, 2, 14);
        LocalDate today = LocalDate.of(2021, 2, 10);
        boolean expResult = true;
        //Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        //Assert
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckEnableTimeTodayEquleEndTime() {
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021, 2, 10);
        LocalDate endTime = LocalDate.of(2021, 2, 14);
        LocalDate today = LocalDate.of(2021, 2, 14);
        boolean expResult = true;
        //Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        //Assert
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckEnableTimeTodayIsBeforeStartTime() {
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021, 2, 10);
        LocalDate endTime = LocalDate.of(2021, 2, 14);
        LocalDate today = LocalDate.of(2021, 1, 3);
        boolean expResult = false;
        //Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        //Assert
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckEnableTimeTodayIsAfterEndTime() {
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021, 2, 10);
        LocalDate endTime = LocalDate.of(2021, 2, 14);
        LocalDate today = LocalDate.of(2021, 2, 15);
        boolean expResult = false;
        //Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        //Assert
        assertEquals(expResult, result);
    }
}
